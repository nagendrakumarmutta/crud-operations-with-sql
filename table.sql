CREATE TABLE product(
    id int not null AUTO_INCREMENT,
    name varchar(255) not null,
    description varchar(255),
    price int,
    primary key(id)
)