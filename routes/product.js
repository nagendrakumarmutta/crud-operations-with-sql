const express = require("express");
const connection = require("../connection");
const router = express.Router();


router.post("/create", (req, res, next)=>{
    let {name, description, price} = req.body;
    const sqlQuery = `insert into product(name, description, price) 
    values("${name}", "${description}", "${price}")`;
    connection.query(sqlQuery, (err, results)=> {
        if(!err){
            return res.status(200).json({message:"product Added Successfully"})
        }else{
            res.status(500).json(err)
        }
    })
});


router.get("/read", (req, res) => {
    const sqlQuery = `select * from product`;
    connection.query(sqlQuery, (err, results)=>{
        if(!err){
            res.status(200).json(results)
        }else{
            res.status(500).json(err)
        }
    })
});


router.patch("/update/:id", (req, res) => {
    const id = req.params.id;
    const {name, description, price} = req.body;
    const sqlQuery = `update product set name="${name}", description = "${description}", price="${price}" where id = ${id}`

    connection.query(sqlQuery, (err, results)=>{
        if(!err){
            if(results.affectedRows == 0){
                res.status(404).json({message:"id does not exist"})
            }else{
                res.status(200).json("updated successfully")
            }
        }else{
            res.status(500).json(err)
        }
    })
});


router.delete("/delete/:id", (req, res) => {
    const id = req.params.id;
    const sqlQuery = `delete from product where id = ${id}`

    connection.query(sqlQuery, (err, results) => {
        if(!err){
            if(results.affectedRows ==0){
                res.status(404).json({message: "id does not exist"});
            }else{
                res.status(200).json({msg:"product deleted successfully"})
            }
        }else{
            res.status(400).json(err)
        }
    })
});


module.exports = router;